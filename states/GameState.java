package mygame.states;

import java.awt.*;
import java.awt.image.*;

import mygame.util.Keyboard;
import mygame.util.Mouse;
import mygame.main.Game;

public abstract class GameState {
    protected Game game;

    public GameState(Game game) {
        this.game = game;
    }

    public abstract void update();
    public abstract void render(Graphics frame);
    public abstract void input(Keyboard keyboard, Mouse mouse);
}
