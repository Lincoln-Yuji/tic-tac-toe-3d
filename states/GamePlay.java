package mygame.states;

import java.awt.*;
import java.awt.image.*;
import java.util.ArrayList;
import java.awt.event.KeyEvent;

import mygame.util.Keyboard;
import mygame.util.Mouse;
import mygame.main.Game;
import mygame.main.Room;

public class GamePlay extends GameState {
    private Room room;

    public GamePlay(Game game) {
        super(game);
        this.room = new Room("Room_0", this.game, this);
    }

    public void update() {
        input(game.getKeyboard(), game.getMouse());
        room.update();
    }

    public void render(Graphics frame) {
        room.render(frame);
    }

    public void input(Keyboard keyboard, Mouse mouse) {
        // Nothing so far...
    }

    public void restartRoom(String name) {
        this.room = new Room(name, this.game, this);
    }
}
