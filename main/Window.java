package mygame.main;

import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.image.*;

public class Window extends Canvas {
    protected Game game;

    private final int WIDTH = 640;
    private final int HEIGHT = 640;

    protected JFrame screen;
    protected BufferedImage panel;

    public Window(Game game) {
        this.game = game;
        this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        this.screen = new JFrame();
        this.screen.add(this);
        this.screen.pack();
        this.screen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.screen.setVisible(true);
        this.screen.setResizable(false);
        this.screen.setLocationRelativeTo(null);
        this.setFocusable(true);
        this.requestFocus();
        this.panel = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        this.addKeyListener(game.keyboard);
        this.addMouseListener(game.mouse);
        this.addMouseMotionListener(game.mouse);
    }

    public int getWidth() {
        return WIDTH;
    }

    public int getHeight() {
        return HEIGHT;
    }
}
