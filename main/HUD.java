package mygame.main;

import java.awt.*;
import java.awt.image.*;

public class HUD {
    Room master;

    public HUD(Room master) {
        this.master = master;
    }

    public void render(Graphics frame) {
        Font current_font = new Font("Serif", Font.BOLD, 25);
        frame.setFont(current_font);
        frame.setColor(Color.WHITE);

        int box_width = getStringWidth("Turn: " + master.getTurnCounter(), current_font, frame) + 50;
        int box_height = 80;
        frame.drawRect(15, 70, box_width, box_height);

        frame.drawString("Turn: " + master.getTurnCounter(), 30, 100);
        frame.drawString("Player: " + master.getTurnPlayer(), 30, 135);
        if (master.getEndGame()) {
            frame.setFont(new Font("Serif", Font.BOLD, 15));
            frame.drawString("Press ENTER to restart", 10, 200);
        }
    }

    private int getStringWidth(String text, Font font, Graphics frame) {
        return frame.getFontMetrics(font).stringWidth(text);
    }
    // private int getStringHeight(String text, Font font, Graphics frame) {
    //     return frame.getFontMetrics(font).stringHeight(text);
    // }
}
