package mygame.main;

import java.awt.*;
import java.awt.image.*;
import java.util.*;

import mygame.util.Mouse;
import mygame.util.Keyboard;
import mygame.graphics.SpriteManager;
import mygame.graphics.RoomLayoutManager;
import mygame.states.*;

public class Game implements Runnable {
    protected Window window;
    protected Keyboard keyboard;
    protected Mouse mouse;

    private boolean is_running = false;
    private double FPS = 60.0;
    private Thread thread;

    private ArrayList <GameState> states;
    private int current_state_index;

    public static void main(String[] args) {
        Game game = new Game();
        game.start();
    }

    public Game() {
        // Initializing the Game Resources:
        this.keyboard = new Keyboard();
        this.mouse = new Mouse();
        this.window = new Window(this);
        SpriteManager.init();
        RoomLayoutManager.init();

        // Initializaing the Array List of States
        this.states = new ArrayList <GameState> ();
        this.states.add(new GamePlay(this));
        this.current_state_index = 0;
    }

    public synchronized void start() {
        this.thread = new Thread(this, "GameThread");
        this.is_running = true;
        this.thread.start();
    }

    public void stop() {
        try {
            thread.join();
            this.is_running = false;
        }
        catch (Exception error) {
            error.printStackTrace();
        }
    }

    public void run() {
        long last_time = System.nanoTime();
        double polar = this.FPS / 1000000000;
        double delta = 0;
        while (this.is_running) {
            long now = System.nanoTime();
            delta += (now - last_time) * polar;
            last_time = now;
            if (delta >= 1) {
                this.update();
                this.render();
                delta--;
            }
        }
        this.stop();
    }

    private void update() {
        keyboard.update();
        if (!(current_state_index < 0) && current_state_index < states.size()) {
            states.get(current_state_index).update();
        }
    }

    private void render() {
        BufferStrategy buffer_st = window.getBufferStrategy();
        if (buffer_st == null) {
            window.createBufferStrategy(3);
            return;
        }
        Graphics frame = window.panel.getGraphics();
        frame.setColor(new Color(20, 20, 20));
        frame.fillRect(0, 0, window.getWidth(), window.getHeight());

        if (!(current_state_index < 0) && current_state_index < states.size()) {
            states.get(current_state_index).render(frame);
        }

        frame = buffer_st.getDrawGraphics();
        frame.drawImage(window.panel, 0, 0, window.getWidth(), window.getHeight(), null);
        frame.dispose();
        buffer_st.show();
    }


    public Keyboard getKeyboard() {
        return keyboard;
    }

    public Mouse getMouse() {
        return mouse;
    }

    public void setState(int i) {
        this.current_state_index = i;
    }

    public int getStateIndex() {
        return this.current_state_index;
    }
}
