package mygame.main;

import java.awt.*;
import java.awt.image.*;
import java.util.ArrayList;

import mygame.entities.*;
import mygame.tiles.*;
import mygame.graphics.RoomLayoutManager;
import mygame.graphics.SpriteManager;
import mygame.util.*;
import mygame.states.GamePlay;

public class Room {
    private ArrayList <Entity> entities;
    private Game game;
    private GamePlay manager;
    private HUD hud;

    private BufferedImage map;
    private Vector2i cam;

    private Tabuleiro tabuleiro;
    private Vector2i mousePos;
    public boolean mouse_clicked;
    public int turnValue;

    private int turnCounter;
    private int turnPlayer;
    private boolean end_game = false;

    public Room(String name, Game game, GamePlay manager) {
        this.game = game;
        this.manager = manager;
        this.cam = new Vector2i();
        this.entities = new ArrayList <Entity> ();
        this.mousePos = new Vector2i();
        this.tabuleiro = new Tabuleiro(4, this);
        entities.add(tabuleiro);
        turnValue = 1;
        turnCounter = 1;
        turnPlayer = 1;
        this.hud = new HUD(this);
    }

    public void update() {
        input(game.getMouse(), game.getKeyboard());
        if (!end_game) {
            for (int i = 0; i < entities.size(); i++) {
                Entity tmpObject = entities.get(i);
                tmpObject.update();
            }
            if (tabuleiro.verifyVictory()) {
                end_game = true;
                int[][] victoryArray = tabuleiro.howVictoryHappened();
                this.previousTurn();
                for (int[] tripla : victoryArray) {
                    tabuleiro.setMatrixValue(tripla[0], tripla[1], tripla[2], turnValue * 2);
                }
            }
        }
    }

    public void render(Graphics frame) {
        for (int i = 0; i < entities.size(); i++) {
            Entity tmpObject = entities.get(i);
            tmpObject.render(frame);
        }

        String cell_info = "Cell: ???";
        if (!(tabuleiro.getFocusX() == -1 || tabuleiro.getFocusY() == -1 || tabuleiro.getFocusZ() == -1)) {
            cell_info = String.format("Cell: %d, %d, %d", tabuleiro.getFocusX(), tabuleiro.getFocusY(), tabuleiro.getFocusZ());
        }
        frame.setColor(Color.WHITE);
        frame.setFont(new Font("Serif", Font.BOLD, 20));
        frame.drawString(cell_info, 10, 25);
        hud.render(frame);
    }

    public void input(Mouse mouse, Keyboard keyboard) {
        this.mousePos = new Vector2i(mouse.getMouseX(), mouse.getMouseY());
        if (mouse.getMouseLeft()) {
            this.mouse_clicked = true;
        }
        else {
            this.mouse_clicked = false;
        }
        if (end_game && keyboard.ENTER_justPressed) {
            manager.restartRoom("Room_0");
        }
    }

    public int getCamX() {
        return cam.x;
    }
    public int getCamY() {
        return cam.y;
    }

    public int getTurnPlayer() {
        return turnPlayer;
    }
    public int getTurnCounter() {
        return turnCounter;
    }

    public boolean getEndGame() {
        return end_game;
    }

    public Vector2i getMousePos() {
        return mousePos;
    }

    public void nextTurn() {
        turnValue *= -1;
        turnCounter++;
        turnPlayer = turnPlayer == 2 ? 1 : 2;
    }
    private void previousTurn() {
        turnValue *= -1;
        turnCounter--;
        turnPlayer = turnPlayer == 2 ? 1 : 2;
    }
}
