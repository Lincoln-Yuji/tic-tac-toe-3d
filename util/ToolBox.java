package mygame.util;

import java.awt.Rectangle;
import java.lang.Math;

public class ToolBox {

    // === Clamp method parameters === //
    // ===============================
    // # int n: the term to be clamped;
    // # int min: the inferior limit of the clamp;
    // # int max: the superior limit of the clamp;
    // ===============================
    public static int clamp(int n, int min, int max) {
        return Integer.max(min, Integer.min(n, max));
    }

    // === Distnce between two points defined on R2 === //
    // ===============================
    // # Vector2i p: one of the segment of rect's extreme points;
    // # Vector2i q: ine of the segment of rect's extreme points;
    // ===============================
    public static double pointDistance(Vector2i p, Vector2i q) {
        return Math.sqrt(Math.pow(p.x - q.x, 2) + Math.pow(p.y - q.y, 2));
    }

    // === Collision between two Rectangles === //
    // ===============================
    // # Vector2i v1: the superior left vertice coordinate of the fisrt Rectangle;
    // # int w1: the width of the first Rectangle;
    // # int h1: the height of the first Rectangle;
    // # Vector2i v2: the superior left vertice coordinate of the second Rectangle;
    // # int w2: the width of the second Rectangle;
    // # int h2: the height of the second Rectangle;
    // ===============================
    public static boolean collisionRR(Vector2i v1, int w1, int h1, Vector2i v2, int w2, int h2) {
        Rectangle r1 = new Rectangle(v1.x, v1.y, w1, h1);
        Rectangle r2 = new Rectangle(v2.x, v2.y, w2, h2);
        return r1.intersects(r2);
    }

    // === Collision between two Circles === //
    // ===============================
    // # Vector2i c1: the center point  coordinate of the first Circle;
    // # int r1: the radius of the first Circle;
    // # Vector2i c2: the center point  coordinate of the second Circle;
    // # int r2: the radius of the second Circle;
    // ===============================
    public static boolean collisionCC(Vector2i c1, int r1, Vector2i c2, int r2) {
        double dist = pointDistance(c1, c2);
        return dist < r1 + r2;
    }

    // === Collision between a Rectangle and a Circle === //
    // ===============================
    // # Vector2i p: the superior left vertice coordinate of the Rectangle;
    // # int width: the width of the Rectangle;
    // # int height: the height of the Rectangle;
    // # Vector2i c: the center point of the Circle;
    // # int r: the radius of the Circle;
    public static boolean collisionRC(Vector2i p, int width, int height, Vector2i c, int r) {
        Vector2i q = new Vector2i(clamp(c.x, p.x, p.x + width), clamp(c.y, p.y, p.y + height));
        double dist = pointDistance(q, c);
        return dist < r;
    }
}
