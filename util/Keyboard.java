package mygame.util;

import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.util.HashMap;

public class Keyboard implements KeyListener {
    protected HashMap <Integer, Key> keys = new HashMap <Integer, Key> ();

    public boolean W_pressed, A_pressed, S_pressed, D_pressed;
    public boolean W_justPressed, A_justPressed, S_justPressed, D_justPressed;
    public boolean ENTER_pressed, ENTER_justPressed;

    public class Key {
        protected boolean pressed;
        protected boolean justPressed;
        protected int framesPressed;

        public Key(int code) {
            pressed = false;
            justPressed = false;
            keys.put(code, this);
        }

        protected void togglePressed(boolean pressed) {
            this.pressed = pressed;
        }
        protected void toggleJustPressed(boolean status) {
            this.justPressed = status;
        }
    }

    public Keyboard() {
        new Key(KeyEvent.VK_W);
        new Key(KeyEvent.VK_A);
        new Key(KeyEvent.VK_S);
        new Key(KeyEvent.VK_D);
        new Key(KeyEvent.VK_ENTER);
    }


    public void update() {
        W_pressed = keys.get(KeyEvent.VK_W).pressed;
        A_pressed = keys.get(KeyEvent.VK_A).pressed;
        S_pressed = keys.get(KeyEvent.VK_S).pressed;
        D_pressed = keys.get(KeyEvent.VK_D).pressed;
        ENTER_pressed = keys.get(KeyEvent.VK_ENTER).pressed;

        for (Integer i : keys.keySet()) {
            Key k = keys.get(i);
            if (k.pressed) {
                if (k.framesPressed == 0) {
                    k.toggleJustPressed(true);
                }
                else {
                    k.toggleJustPressed(false);
                }
                k.framesPressed = k.framesPressed + 1 > 60 ? 60 : k.framesPressed + 1;
            }
            else {
                k.framesPressed = 0;
            }
        }

        W_justPressed = keys.get(KeyEvent.VK_W).justPressed;
        A_justPressed = keys.get(KeyEvent.VK_A).justPressed;
        S_justPressed = keys.get(KeyEvent.VK_S).justPressed;
        D_justPressed = keys.get(KeyEvent.VK_D).justPressed;
        ENTER_justPressed = keys.get(KeyEvent.VK_ENTER).justPressed;
    }


    public void keyPressed(KeyEvent event) {
        Key key = keys.get(event.getKeyCode());
        if (key != null) {
            if (key.pressed) {
                key.toggleJustPressed(false);
            }
            else {
                key.toggleJustPressed(true);
            }
            key.togglePressed(true);
        }
    }

    public void keyReleased(KeyEvent event) {
        Key key = keys.get(event.getKeyCode());
        if (key != null) {
            key.togglePressed(false);
            key.toggleJustPressed(false);
        }
    }

    public void keyTyped(KeyEvent event) {
        // Empty...
    }
}
