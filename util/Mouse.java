package mygame.util;

import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;

public class Mouse implements MouseListener, MouseMotionListener {

    private int mouse_x, mouse_y;
    private boolean mouse_left, mouse_right;

    public Mouse() {
        mouse_left = false;
        mouse_right = false;
        mouse_x = 0;
        mouse_y = 0;
    }

    public void mousePressed(MouseEvent event) {
        switch (event.getButton()) {
            case MouseEvent.BUTTON1: // Left Button
                mouse_left = true;
                break;
            case MouseEvent.BUTTON3: // Right Button
                mouse_right = true;
                break;
        }
    }

    public void mouseReleased(MouseEvent event) {
        switch (event.getButton()) {
            case MouseEvent.BUTTON1: // Left Button
                mouse_left = false;
                break;
            case MouseEvent.BUTTON3: // Right Button
                mouse_right = false;
                break;
        }
    }

    public void mouseMoved(MouseEvent event) {
        mouse_x = event.getX();
        mouse_y = event.getY();
    }

    public int getMouseX() {
        return mouse_x;
    }

    public int getMouseY() {
        return mouse_y;
    }

    public boolean getMouseLeft() {
        return mouse_left;
    }

    public boolean getMouseRight() {
        return mouse_right;
    }

    public void mouseDragged(MouseEvent event) { /* Empty */ }
    public void mouseClicked(MouseEvent event) { /* Empty */ }
    public void mouseEntered(MouseEvent event) { /* Empty */ }
    public void mouseExited(MouseEvent event)  { /* Empty */ }
}
