package mygame.util;

public class Vector2i {
    public int x, y;

    public Vector2i() {
        this.x = 0;
        this.y = 0;
    }
    public Vector2i(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public Vector2i(Vector2i v) {
        this.x = v.x;
        this.y = v.y;
    }

    public static Vector2i sum(Vector2i v1, Vector2i v2) {
        return (new Vector2i(v1.x + v2.x, v1.y + v2.y));
    }
}
