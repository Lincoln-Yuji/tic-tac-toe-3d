package mygame.graphics;

import java.util.HashMap;
import java.awt.*;
import java.awt.image.*;
import java.io.IOException;
import java.io.File;
import javax.imageio.ImageIO;

public class RoomLayoutManager {
    private static HashMap <String, BufferedImage> rooms;

    public static void init() {
        rooms = new HashMap <String, BufferedImage> ();
        File folder = new File("assets/rooms");
        for (File file : folder.listFiles()) {
            try {
                rooms.put(file.getName().replaceAll(".png", ""), ImageIO.read(file));
            }
            catch (IOException error) {
                System.out.println("[ FAIL! ] Exception reading " + file.getName());
            }
        }
        System.out.println("Finished reading Rooms Layouts");
    }

    public static BufferedImage getLayout(String name) {
        BufferedImage layout = rooms.get(name);
        return layout;
    }
}
