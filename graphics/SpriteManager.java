package mygame.graphics;

import java.util.HashMap;
import java.awt.*;
import java.awt.image.*;
import java.io.IOException;
import java.io.File;
import javax.imageio.ImageIO;

public class SpriteManager {
    private static HashMap <String, BufferedImage> sprites;

    public static void init() {
        sprites = new HashMap <String, BufferedImage> ();
        File folder = new File("assets/sprites");
        for (File file : folder.listFiles()) {
            try {
                sprites.put(file.getName().replaceAll(".png", ""), ImageIO.read(file));
            }
            catch (IOException error) {
                System.out.println("[ FAIL! ] Exception reading " + file.getName());
            }
        }
        System.out.println("Finished reading sprite files");
    }

    public static BufferedImage getSprite(String name, int x, int y, int width, int height) {
        BufferedImage img = sprites.get(name);
        return img.getSubimage(x, y, width, height);
    }
}
