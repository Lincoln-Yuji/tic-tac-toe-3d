package mygame.entities;

import java.awt.*;
import java.awt.image.*;
import java.util.ArrayList;

import mygame.main.Room;
import mygame.tiles.*;
import mygame.graphics.SpriteManager;
import mygame.util.*;

public class Tabuleiro extends Entity {
    private Tile[][][] visual_matrix;
    private int[][][] matriz;
    private int[][][] linhas;
    private int[][] diagonais_planares;
    private int[] diagonais_do_cubo;
    private int dim;

    public static final Vector2i origin0 = new Vector2i(320 + 64, 64);
    public static final Vector2i origin1 = new Vector2i(320 + 64, 64 + 32 * 4);
    public static final Vector2i origin2 = new Vector2i(320 + 64, 64 + 32 * 4 * 2);
    public static final Vector2i origin3 = new Vector2i(320 + 64, 64 + 32 * 4 * 3);

    private int xSelected, ySelected, zSelected;

    public Tabuleiro(int dim, Room master) {
        super(0, 0, master);
        this.dim = dim;
        visual_matrix = new Tile[dim][dim][dim];
        matriz = new int[dim][dim][dim];
        linhas = new int[dim][dim][3];
        diagonais_planares = new int[dim][6];
        diagonais_do_cubo = new int[4];
        chargeTabuleiro();
    }

    public void update() {
        Vector2i mouse = new Vector2i(master.getMousePos());
        Vector2i tmpOrigin = getOrigin(mouse);
        String cell_position;
        Vector2i grid_pos = new Vector2i((mouse.x / Tile.SIZE_X) * Tile.SIZE_X, (mouse.y / Tile.SIZE_Y) * Tile.SIZE_Y);
        int color = Tile.TILE_MASK.getRGB(mouse.x % Tile.SIZE_X, mouse.y % Tile.SIZE_Y);

        xSelected = (grid_pos.y - tmpOrigin.y) / Tile.SIZE_Y + (grid_pos.x - tmpOrigin.x) / Tile.SIZE_X;
        ySelected = (grid_pos.y - tmpOrigin.y) / Tile.SIZE_Y - (grid_pos.x - tmpOrigin.x) / Tile.SIZE_X;

        if (color == 0xFF00FF00) { xSelected += 0; ySelected -= 1; }
        if (color == 0xFFFF0000) { xSelected -= 1; ySelected += 0; }
        if (color == 0xFF0000FF) { xSelected += 0; ySelected += 1; }
        if (color == 0xFFFFFF00) { xSelected += 1; ySelected += 0; }

        if (!validPosition(xSelected, ySelected, zSelected)) {
            xSelected = -1; ySelected = -1; zSelected = -1;
        }
        else {
            if (master.mouse_clicked && matriz[xSelected][ySelected][zSelected] == 0) {
                tick(xSelected, ySelected, zSelected, master.turnValue);
                master.nextTurn();
            }
        }

    }

    public void render(Graphics frame) {
        for (int x = 0; x < this.dim; x++) {
            for (int y = 0; y < this.dim; y++) {
                for (int z = 0; z < this.dim; z++) {
                    Tile tmpTile = visual_matrix[x][y][z];
                    if (tmpTile != null) { tmpTile.render(frame); }
                    if (matriz[x][y][z] == +1) { Symbol.render("X", x, y, z, frame); }
                    if (matriz[x][y][z] == -1) { Symbol.render("O", x, y, z, frame); }
                    if (matriz[x][y][z] == +2) { Symbol.renderRedX(x, y, z, frame); }
                    if (matriz[x][y][z] == -2) { Symbol.renderRedO(x, y, z, frame); }
                }
            }
        }
    }

    private Vector2i getOrigin(Vector2i mouse) {
        if (mouse.y < Tabuleiro.origin0.y) { zSelected = -1; return new Vector2i(); }
        if (mouse.y < Tabuleiro.origin1.y) { zSelected = +0; return new Vector2i(Tabuleiro.origin0.x, Tabuleiro.origin0.y); }
        if (mouse.y < Tabuleiro.origin2.y) { zSelected = +1; return new Vector2i(Tabuleiro.origin1.x, Tabuleiro.origin1.y); }
        if (mouse.y < Tabuleiro.origin3.y) { zSelected = +2; return new Vector2i(Tabuleiro.origin2.x, Tabuleiro.origin2.y); }
        zSelected = 3;
        return new Vector2i(Tabuleiro.origin3.x, Tabuleiro.origin3.y);
    }

    public int getFocusX() {
        return xSelected;
    }
    public int getFocusY() {
        return ySelected;
    }
    public int getFocusZ() {
        return zSelected;
    }

    private void chargeTabuleiro() {
        for (int x = 0; x < this.dim; x++) {
            for (int y = 0; y < this.dim; y++) {
                for (int z = 0; z < this.dim; z++) {
                    Vector2i render = new Vector2i();
                    render.x = origin0.x + (x - y) * Tile.SIZE_X / 2;
                    render.y = origin0.y + (x + y) * Tile.SIZE_Y / 2 + 32 * 4 * z;
                    this.visual_matrix[x][y][z] = new TileSlot(render.x, render.y, this.master);
                }
            }
        }
    }

    public boolean validPosition(int x, int y, int z) {
        return ((!(x < 0) && x < 4) && (!(y < 0) && y < 4) && (!(z < 0) && z < 4));
    }

    public void tick(int x, int y, int z, int value) {
        matriz[x][y][z] = value;
        linhas[x][y][0] += value;
        linhas[y][z][1] += value;
        linhas[x][z][2] += value;

        if (x == y) { diagonais_planares[z][0] += value; }
        if (x == 3 - y) { diagonais_planares[z][1] += value; }
        if (y == z) { diagonais_planares[x][2] += value; }
        if (y == 3 - z) { diagonais_planares[x][3] += value; }
        if (x == z) { diagonais_planares[y][4] += value; }
        if (x == 3 - z) { diagonais_planares[y][5] += value; }

        if (x == y && x == z) { diagonais_do_cubo[0] += value; }
        if (x == y && x == 3 - z) { diagonais_do_cubo[1] += value; }
        if (y == z && y == 3 - x) { diagonais_do_cubo[2] += value; }
        if (x == z && x == 3 - y) { diagonais_do_cubo[3] += value; }
    }

    public boolean verifyVictory() {
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                for (int k = 0; k < 3; k++) {
                    if (Math.abs(linhas[i][j][k]) == dim) {
                        return true;
                    }
                }
            }
        }
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < 6; j++) {
                if (Math.abs(diagonais_planares[i][j]) == 4) {
                    return true;
                }
            }
        }
        for (int i = 0; i < 4; i++) {
            if (Math.abs(diagonais_do_cubo[i]) == 4) {
                return true;
            }
        }
        return false;
    }

    public int[][] howVictoryHappened() {
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                for (int k = 0; k < 3; k++) {
                    if (Math.abs(linhas[i][j][k]) == dim) {
                        if (k == 0) { return new int[][] {{i, j, 0}, {i, j, 1}, {i, j, 2}, {i, j, 3}}; }
                        if (k == 1) { return new int[][] {{0, i, j}, {1, i, j}, {2, i, j}, {3, i, j}}; }
                        if (k == 2) { return new int[][] {{i, 0, j}, {i, 1, j}, {i, 2, j}, {i, 3, j}}; }
                    }
                }
            }
        }
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < 6; j++) {
                if (Math.abs(diagonais_planares[i][j]) == 4) {
                    if (j == 0) { return new int[][] {{0, 0, i}, {1, 1, i}, {2, 2, i}, {3, 3, i}}; }
                    if (j == 1) { return new int[][] {{0, 3, i}, {1, 2, i}, {2, 1, i}, {3, 0, i}}; }
                    if (j == 2) { return new int[][] {{i, 0, 0}, {i, 1, 1}, {i, 2, 2}, {i, 3, 3}}; }
                    if (j == 3) { return new int[][] {{i, 0, 3}, {i, 1, 2}, {i, 2, 1}, {i, 3, 0}}; }
                    if (j == 4) { return new int[][] {{0, i, 0}, {1, i, 1}, {2, i, 2}, {3, i, 3}}; }
                    if (j == 5) { return new int[][] {{3, i, 0}, {2, i, 1}, {1, i, 2}, {0, i, 3}}; }
                }
            }
        }
        for (int i = 0; i < 4; i++) {
            if (Math.abs(diagonais_do_cubo[i]) == 4) {
                if (i == 0) { return new int[][] {{0, 0, 0}, {1, 1, 1}, {2, 2, 2}, {3, 3, 3}}; }
                if (i == 1) { return new int[][] {{0, 0, 3}, {1, 1, 2}, {2, 2, 1}, {3, 3, 0}}; }
                if (i == 2) { return new int[][] {{3, 0, 0}, {2, 1, 1}, {1, 2, 2}, {0, 3, 3}}; }
                if (i == 4) { return new int[][] {{0, 3 ,0}, {1, 2, 1}, {2, 1, 2}, {3, 0, 3}}; }
            }
        }
        return null;
    }

    public void setMatrixValue(int x, int y, int z, int v) {
        matriz[x][y][z] = v;
    }
}

abstract class Symbol {
    static BufferedImage xSymbol = SpriteManager.getSprite("spr_Symbols", 0, 0, Tile.SIZE_X, Tile.SIZE_Y);
    static BufferedImage oSymbol = SpriteManager.getSprite("spr_Symbols", 64, 0, Tile.SIZE_X, Tile.SIZE_Y);
    static BufferedImage xSymbolRed = SpriteManager.getSprite("spr_Symbols", 128, 0, Tile.SIZE_X, Tile.SIZE_Y);
    static BufferedImage oSymbolRed = SpriteManager.getSprite("spr_Symbols", 192, 0, Tile.SIZE_X, Tile.SIZE_Y);

    static void render(String symbol, int x, int y, int z, Graphics frame) {
        int refx = Tabuleiro.origin0.x + (x - y) * Tile.SIZE_X / 2;
        int refy = Tabuleiro.origin0.y + (x + y) * Tile.SIZE_Y / 2 + 32 * 4 * z;
        if (symbol.equals("X")) {
            frame.drawImage(xSymbol, refx, refy, null);
        }
        if (symbol.equals("O")) {
            frame.drawImage(oSymbol, refx, refy, null);
        }
    }

    static void renderRedX(int x, int y, int z, Graphics frame) {
        int refx = Tabuleiro.origin0.x + (x - y) * Tile.SIZE_X / 2;
        int refy = Tabuleiro.origin0.y + (x + y) * Tile.SIZE_Y / 2 + 32 * 4 * z;
        frame.drawImage(xSymbolRed, refx, refy, null);
    }
    static void renderRedO(int x, int y, int z, Graphics frame) {
        int refx = Tabuleiro.origin0.x + (x - y) * Tile.SIZE_X / 2;
        int refy = Tabuleiro.origin0.y + (x + y) * Tile.SIZE_Y / 2 + 32 * 4 * z;
        frame.drawImage(oSymbolRed, refx, refy, null);
    }
}
