package mygame.entities;

import java.awt.*;
import java.awt.image.*;

import mygame.util.Vector2i;
import mygame.main.Room;

public abstract class Entity {
    Room master;
    Vector2i pos;

    public Entity(int x, int y, Room master) {
        this.pos = new Vector2i(x, y);
        this.master = master;
    }

    public abstract void update();
    public abstract void render(Graphics frame);
}
