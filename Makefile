build:
	javac -d bin main/*.java entities/*.java graphics/*.java util/*.java tiles/*.java states/*.java
	cp -r assets bin

clean:
	rm -rf bin
