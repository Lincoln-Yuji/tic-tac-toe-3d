package mygame.tiles;

import java.awt.*;
import java.awt.image.*;

import mygame.main.Room;

public class TileSlot extends Tile {
    public TileSlot(int x, int y, Room room) {
        super(x, y);
        master = room;
    }

    public void render(Graphics frame) {
        int render_x = pos.x - master.getCamX();
        int render_y = pos.y - master.getCamY();
        frame.drawImage(TILE_SLOT, render_x, render_y, null);
    }

    public boolean isSolid() {
        return true;
    }
}
