package mygame.tiles;

import java.awt.*;
import java.awt.image.*;

import mygame.util.Vector2i;
import mygame.graphics.SpriteManager;
import mygame.main.Room;

public abstract class Tile {
    public static final int SIZE_X = 64; // Em pixels
    public static final int SIZE_Y = 32; // Em pixels

    protected static BufferedImage TILE_SLOT = SpriteManager.getSprite("spr_Tiles", 0, 0, SIZE_X, SIZE_Y);
    protected static BufferedImage TILE_HIGHLIGHT = SpriteManager.getSprite("spr_Tiles", 64, 0, SIZE_X, SIZE_Y);
    public static BufferedImage TILE_MASK = SpriteManager.getSprite("spr_Tiles", 0, 32, SIZE_X, SIZE_Y);

    protected Vector2i pos;
    protected Room master;

    public Tile(int x, int y) {
        pos = new Vector2i(x, y);
    }

    public abstract void render(Graphics frame);
    public abstract boolean isSolid();
}
